'use strict';

angular.module('meanApp')
    .service('CurrentTask', function ($http) {

        console.log("CurrentTask is called");

        var user;

        $http({method: 'GET', url: '/api/users/default'}).
            success(function (data, status, headers, config) {
                user = data;
            });

        this.currentTaskIndex = 0;
        this.bundlesForTask = [];

        this.getNextTaskId = function () {
            if (this.currentTaskIndex + 1 > user.tasks.length - 1) {
                return null;
            } else {
                this.currentTaskIndex += 1;
                return user.tasks[this.currentTaskIndex];
            }
        }

        this.submitBundles = function (bundles) {
            this.bundlesForTask = bundles;
        };
    });