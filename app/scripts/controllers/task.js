/**
 * Created by horatiu on 26/02/14.
 */
'use strict';

//angular.module('meanApp')
//    .value('appConstants', {
//        BUNDLE_SIZE: 5
//    });

angular.module('meanApp')
    .controller('TaskCtrl', function ($scope, $rootScope, $routeParams, $modal, $sce, $http, $location, CurrentTask) {

        var BUNDLE_SIZE = 5;

        //Set task state
        // alternative is task = 'review'
        $scope.taskState = 'build';

        // Load query data
        // from API
        $scope.data = null;
        // Array with documents in
        // the current bundle
        $scope.currentBundle = [];
        $scope.currentBundleTypes = [];

        // All bundles
        $scope.allBundles = [];

        // Get data from api
        var target = '/api/task';
//        var initialTaskId = CurrentTask.getInitialTaskId();

//        if (initialTaskId) {
//            target += '?qid=' + initialTaskId;
//        }

        if ($routeParams.qid) {
            target += '?qid=' + $routeParams.qid;
        }

        $http.get(target).success(function (data) {
            $scope.data = data;
        });

        // Options for the tile
        // environment
        $scope.bundleGridsterOpts = {
            minRows: 1,
            maxRows: 1,
            columns: BUNDLE_SIZE * 2,
            colWidth: 'auto', // can be an integer or 'auto'.  'auto' uses the pixel width of the element divided by 'columns'
            rowHeight: 'match', // can be an integer or 'match'.  Match uses the colWidth, giving you square widgets.
            margins: [5, 5], // the pixel distance between each widget
            defaultSizeX: 2, // the default width of a gridster item, if not specifed
            defaultSizeY: 1, // the default height of a gridster item, if not specified
            resizable: {
                enabled: undefined
            },
            draggable: {
                enabled: false
            }
        }

        $scope.verticalGridsterOpts = {
            minRows: 1, // the minimum height of the grid, in rows
            maxRows: 10,
            columns: 8, // the width of the grid, in columns
            maxColumns: 12,
            colWidth: 'auto', // can be an integer or 'auto'.  'auto' uses the pixel width of the element divided by 'columns'
            rowHeight: 'match', // can be an integer or 'match'.  Match uses the colWidth, giving you square widgets.
            margins: [10, 10], // the pixel distance between each widget
            defaultSizeX: 2, // the default width of a gridster item, if not specifed
            defaultSizeY: 2, // the default height of a gridster item, if not specified
            mobileBreakPoint: 600, // if the screen is not wider that this, remove the grid layout and stack the items
            resize: {
                enabled: true,
                start: function (event, uiWidget, $element) {
                    console.log($element);
                    console.log(uiWidget);
                }, // optional callback fired when resize is started,
                resize: function (event, uiWidget, $element) {
                }, // optional callback fired when item is resized,
                stop: function (event, uiWidget, $element) {
                }
            },
            draggable: {
                enabled: true,
                start: function (event, uiWidget, $element) {
                    console.log("drag start");
                } // optional callback fired when resize is started,
            }
        };

        // Get the size of a vertical item based on the
        // auto dimensions set by the gridster
        $scope.getItemSize = function () {
            // padding for the button is 10px
            if (!$scope.itemSize) {
                $scope.itemSize = $rootScope.gridsterColWidth * 0.85;
            }
            return $scope.itemSize;
//            return ($rootScope.gridsterColWidth) - 25;
        }

        // Push new document onto current bundles
        // if bundle size constraints are met
        $scope.addToCurrentBundle = function (doc) {
            if ($scope.currentBundle.length >= BUNDLE_SIZE) {
                alert('Bundle is full');
            } else {
                if ($scope.currentBundle.indexOf(doc) != -1) {
                    alert('Item already in bundle');
                } else {
                    $scope.currentBundle.push(doc);
                    $scope.currentBundleTypes.push(doc.__metadata.type);
                }
            }
        }

        // Remove document @index from current bundle
        $scope.removeFromCurrentBundle = function(index) {
            $scope.currentBundle.splice(index, 1);
            $scope.currentBundleTypes.splice(index, 1);
        }

        $scope.removeDocumentFromBundle = function(bundleIndex, docIndex) {
            // bundle index is the index of the bundle in allBundles
            // docIndex is the index of the document in the bundle
            $scope.allBundles[bundleIndex].splice(docIndex, 1);

            // If all documents from a bundle have been removed,
            // remove that bundle
            if ($scope.allBundles[bundleIndex].length <= 0) {
                $scope.allBundles.splice(bundleIndex, 1);
            }
        }

        // Submit bundle to all bundles
        $scope.submitCurrentBundle = function() {

            if (!$scope.currentBundle.length) {
                alert("Your current bundle is empty.");
            } else {
                var confirmation = confirm("Submit your current bundle?");
                if (confirmation) {
                    $scope.allBundles.push($scope.currentBundle);
                    //Empty current bundle
                    $scope.currentBundle = [];
                    $scope.currentBundleTypes = [];
                    console.log($scope.allBundles);
                }
            }
        }

        // Set view switcher
        // to build mode
        // -- go back, basically
        $scope.resetTaskState = function () {
            $scope.taskState = 'build';
        }

        // Set view switcher
        // to review mode
        $scope.reviewAllBundles = function () {
            $scope.taskState = 'review';
        }

        $scope.navigateToNextTask = function () {
            var nextTaskId = CurrentTask.getNextTaskId();
            if (nextTaskId) {
                var confirmation = confirm("Go to next task?")
                if (confirmation) {
                    //Submit bundles for review
                    CurrentTask.submitBundles($scope.allBundles);

                    //Reset switcher state
                    $scope.resetTaskState();

                    //Navigate to next task
                    var path = '/task?qid=' + nextTaskId;
                    $location.url(path);
                }
            } else {
                alert("All done!")
            }
        }


        // Navigate to next task
        $scope.showDocumentDetails = function(doc) {
            var url;
            if (doc.__metadata.type === 'NewsResult' ||
                doc.__metadata.type === 'WebResult') {
                url = doc.Url;
            } else if (doc.__metadata.type === 'ImageResult') {
                url = doc.DisplayUrl;
            } else if (doc.__metadata.type === 'VideoResult') {
                var videoId = doc.MediaUrl.match(/(youtu\.be\/|[?&]v=)([^&]+)/);
                if (videoId) {
                    url = '//youtube.com/embed/' + videoId[2] + '?rel=0';
                } else {
                    console.log('Not a Youtube video');
                    url = doc.MediaUrl;
                }
            }
            $scope.open(url);
        }

        // Modal
         $scope.open = function (url) {

             if (url.indexOf('http://') === -1) {
                 url = 'http://' + url;
             }

             console.log(url);
             $scope.modalSource = $sce.trustAsResourceUrl(url);


             var modalInstance = $modal.open({
                templateUrl: 'myModalContent.html',
                controller: ModalInstanceCtrl,
                resolve: {
                    source: function () {
                        return $scope.modalSource;
                    }
                }
             });

//             modalInstance.result.then(function (selectedItem) {
//                 $scope.selected = selectedItem;
//             }, function () {
//                 $log.info('Modal dismissed at: ' + new Date());
//             });
         };

        // Please note that $modalInstance represents a modal window (instance) dependency.
        // It is not the same as the $modal service used above.

        var ModalInstanceCtrl = function ($scope, $modalInstance, source) {
            $scope.source = source;
        };
    });