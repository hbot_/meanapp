'use strict';

angular.module('meanApp')
    .controller('MainCtrl', function ($scope, $http) {

        // get a list of queries from
        // the server
        $http.get('/api/allTasks').success(function (allTasks) {

        });


        $http.get('/api/awesomeThings').success(function (awesomeThings) {
            console.log('MAIN');
            $scope.awesomeThings = awesomeThings;
        });
    });
