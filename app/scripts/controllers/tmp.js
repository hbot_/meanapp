'use strict';

angular.module('meanApp')
    .controller('TmpCtrl', function ($scope, $http) {
        console.log('TMP');

        $scope.data = null;

        $http.get('/api/task').success(function (data) {
            $scope.data = data;
            console.log($scope.data);
        });
    });
