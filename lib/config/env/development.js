'use strict';

module.exports = {
  env: 'development',
  mongo: {
    uri: 'mongodb://localhost:8001/bing',
    results_collection: 'results'
  }
};