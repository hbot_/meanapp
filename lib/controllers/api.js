'use strict';

var config = require('../config/config');
var MongoClient = require('mongodb').MongoClient;

//    {   qid: String,
//        qterms: String,
//        qresults: {
//            'web' : Array,
//            'news': Array,
//            'image': Array,
//            'video': Array
//        },
//        qstats: {
//            'WebTotal' : Number,
//            'ImageTotal' : Number,
//            'VideoTotal' : Number,
//            'NewsTotal' : Number
//        }
//    }

/**
 * Get awesome things
 */
exports.awesomeThings = function(req, res) {
  res.json({
      error: "This is just a placeholder"
  });
};

/**
 * Get BING results for given QID
 */

exports.getTaskData = function(req, res) {
    // Get query id parameter
    // from the request URL
    var qid = req.query.qid;

    if (!qid) {
        // Fallback on default QID
        qid = 'trec2009-web-14';
    }

    var dbLocation = config.mongo.uri;
    var collection_name = config.mongo.results_collection;

    MongoClient.connect(dbLocation, function(err, db) {
        if(err) {
            res.json({ error: err, reason: "Could not connect to database" });
            db.close();
        } else {
            var collection = db.collection(collection_name);
            collection.find({'qid':qid}).toArray(function(err, items) {
                if (err) {
                    res.json({error: err, reason: "Could not find results for qid"});
                    console.log(err);
                } else {
                    res.json(items[0]);
                }
                db.close();
            });
        }
    });
};

exports.getAllTasks = function (req, res) {

    res.json({helo: 'world'});
}