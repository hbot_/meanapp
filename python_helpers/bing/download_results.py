from requests.auth import HTTPBasicAuth
from pymongo import MongoClient
import requests
import base64
import simplejson

# http://msdn.microsoft.com/en-us/library/gg193416.aspx#DataMarketOAuth
# http://docs.python-requests.org/en/latest/user/authentication/#basic-authentication

acckey = 'DB2mIL1PPN/sZ8/899dEwhbVW2OFs4ZNy7fCKIyagRY'

class Query:
	"""A simple class that holds query details"""

	def __init__(self, query_line):
		qelems = query_line.strip().split('\t')
		self.orientation = qelems[0]
		self.id = qelems[1]
		self.terms = qelems[2]

\
def get_queries(queries_path):
	queries_file = open(queries_path)
	return [Query(line) for line in queries_file]


def get_results_for_query(query):
	payload = {'Sources' : '\'Web+Image+Video+News\'',  'Query' : '\'' + query + '\'', '$format':'json'} 
	res = requests.get('https://api.datamarket.azure.com/Bing/Search/v1/Composite', params=payload, auth=HTTPBasicAuth('user', acckey))
	return res.text.encode('utf-8')

def parse_results(qid, qterms, results_json):

	json = simplejson.loads(results_json)
	root = json["d"]["results"][0]

	doc = {
		"qid" : qid,
		"qterms" : qterms,
		"qstats" : {
			"WebTotal" : root["WebTotal"],
			"ImageTotal" : root["ImageTotal"],
			"VideoTotal" : root["VideoTotal"],
			"NewsTotal" : root["NewsTotal"]
		},
		"qresults" : {
			"web" : root["Web"],
			"image" : root["Image"],
			"video" : root["Video"],
			"news" : root["News"]
		}	
	}

	return simplejson.dumps(doc)

#############
#############
#### Main ###
#############
#############

client = MongoClient('localhost', 8001)
db = client['bing']
collection = db['results']

for query in get_queries('./queries/query.vertIntent_1.0'):
	print '****', query.terms
	parsed_results = parse_results(query.id, query.terms, get_results_for_query(query.terms))
	
	# Write results to file, for backup
	results_file = open('./results/' + query.id + '.bing.json', 'w')
	results_file.write(parsed_results)
	results_file.close()

	# Upload results to database 
	collection.insert(simplejson.loads(parsed_results))